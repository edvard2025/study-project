"use strict";

let cart = [];
const cartButton = document.getElementById('add');
const productData = {
    name: 'Смартфон Apple iPhone 13',
    color: 'Синий',
    memorySelected: 128,
};

const iconCounter = document.querySelector('.icon__counter');

restoreCart();
changeCounter();
changeButtonState();

function addOrRemoveItemFromCart () {
    const storedCart = localStorage.getItem('cart');
    if (storedCart) {
        const item = JSON.parse(storedCart).find(item => item.name === productData.name)
        if (item) {
            cart = JSON.parse(storedCart).filter(function (item) {
                return item.name !== productData.name
            })
        } else {
            cart.push(productData);
        }
        localStorage.setItem('cart', JSON.stringify(cart));
       } else {
        cart.push(productData);
        localStorage.setItem('cart', JSON.stringify(cart));
    }
}

cartButton.addEventListener ('click', () => {
        addOrRemoveItemFromCart();
        changeCounter();
        changeButtonState();
    }
);

function restoreCart() {
    const storedCart = localStorage.getItem('cart');
    if(storedCart) {
        cart = JSON.parse(storedCart);
    }
}

function changeCounter() {
    if (cart.length) {
        iconCounter.classList.remove('hidden');
    } else {
        iconCounter.classList.add('hidden');
    }
    iconCounter.dataset.count = cart.length;
    let counter = iconCounter.dataset.count;
    iconCounter.innerText = counter;
    if (cart.length > 0) {
        iconCounter.innerText = cart.length;
        iconCounter.classList.add('show-counter');
    } else {
        iconCounter.classList.remove('show-counter');
    }
}

function changeButtonState() {
    const prod = cart.find(prod => prod.name === productData.name)

    if (prod) {
        cartButton.textContent = 'Товар уже в корзине';
        cartButton.classList.add('btn_third');
    } else {
        cartButton.textContent = 'Добавить в корзину';
        cartButton.classList.remove('btn_third');
    }
}

// Form
const EMPTY_INPUT_USER_NAME_ERROR = 'Вы забыли указать имя и фамилию';
const LENGTH_INPUT_USER_NAME_ERROR = 'Имя не может быть короче 2-х символов';
const INPUT_ESTIMATE_ERROR = 'Оценка должна быть от 1 до 5';
// const getInputName = (elem) => elem.getAttribute('name');

const form = document.querySelector('.reviews-form__form');
const inputName = document.querySelector('.review-form__name');
const inputStars = document.querySelector('.review-form__stars');
const formInputs = form.querySelectorAll('input');
console.log(form);

function addErrorClass(input) {
    input.classList.add('error');
}

function removeErrorClass(input) {
    input.addEventListener('focus', () => {
        input.classList.remove('error');
    })
}



form.addEventListener('submit', function(e) {
    e.preventDefault();

    const inputNameValue = inputName.value;
    const inputStarsValue = inputStars.value;
    let text = '';

    if(inputNameValue === "" || inputNameValue.length <= 2) {
        addErrorClass(inputName);
        text = inputNameValue === '' ? EMPTY_INPUT_USER_NAME_ERROR : LENGTH_INPUT_USER_NAME_ERROR;
    }

   
    if (text === '' && ( inputStarsValue === "" || inputStarsValue > 5 || inputStarsValue < 1 || Number.isNaN(+inputStarsValue))) {
        addErrorClass(inputStars);
        text = INPUT_ESTIMATE_ERROR;
    }

    if(text !== '') {
        form.querySelector('.review-form__input.error ~ .reviews-form__error').textContent = text;
    }
});

formInputs.forEach((input) => {
    removeErrorClass(input);
})